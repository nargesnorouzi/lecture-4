package edu.ucsc.cmps121.lecture4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    public Data data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Button back = (Button) findViewById(R.id.back);
        TextView textView_1 = (TextView) findViewById(R.id.textView_1);
        textView_1.setText(MainActivity.text_1);

        TextView textView_2 = (TextView) findViewById(R.id.textView_2);
        //textView_2.setText(getIntent().getExtras().get("Text 2").toString());
        textView_2.setText(getIntent().getExtras().get("Text 2").toString());

        TextView textView_3 = (TextView) findViewById(R.id.textView_3);
        SharedPreferences sharedPreferences = getSharedPreferences("mem", MODE_PRIVATE);
        textView_3.setText(sharedPreferences.getString("Text 3", ""));

        data = Data.getInstance();
        TextView textView_4 = (TextView) findViewById(R.id.textView_4);
        textView_4.setText(data.getString());

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SecondActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
}
