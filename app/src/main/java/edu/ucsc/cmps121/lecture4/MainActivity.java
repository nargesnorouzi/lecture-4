package edu.ucsc.cmps121.lecture4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public  static String text_1 = "";
    public Data data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button myButton = (Button) findViewById(R.id.ok);
        final EditText edit_text_1 = (EditText) findViewById(R.id.editText_1);
        final EditText edit_text_2 = (EditText) findViewById(R.id.editText_2);
        final EditText edit_text_3 = (EditText) findViewById(R.id.editText_3);
        final EditText edit_text_4 = (EditText) findViewById(R.id.editText_4);


        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_1 = edit_text_1.getText().toString();

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                //intent.putExtra("Text 2", edit_text_2.getText().toString());
                Bundle bundle = new Bundle();
                bundle.putString("Text 2", edit_text_2.getText().toString());
                intent.putExtras(bundle);

                SharedPreferences sharedPreferences = getSharedPreferences("mem", MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("Text 3", edit_text_3.getText().toString());
                edit.commit();

                data = Data.getInstance();
                data.setString(edit_text_4.getText().toString());

                startActivity(intent);
            }
        });
    }
}
