package edu.ucsc.cmps121.lecture4;

/**
 * Created by nargesnorouzi on 2017-10-10.
 */

public class Data {
    private String string;
    private static final Data objectHolder = new Data();
    public String getString(){
        return objectHolder.string;
    }
    public void setString(String str){
        string = str;
    }
    public static Data getInstance() {
        return objectHolder;
    }
}
